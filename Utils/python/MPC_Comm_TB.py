import rospy
from geometry_msgs.msg import Twist
from nav_msgs.msg import Odometry
from math import sqrt, pow, atan2, pi, cos, sin
from tf.transformations import euler_from_quaternion
import socket


class MoveAround():
	def __init__(self):
		# initiliaze
		rospy.init_node('MoveAround', anonymous=False)

		# Init pose values
		self.pose = {'x': 0, 'y': 0, 'theta': 0}

		self.r = rospy.Rate(10)
		self.cmd_vel = rospy.Publisher('cmd_vel_mux/input/navi', Twist, queue_size=10)

		# UDP
		self.udp_init()

		# Create subscriber which follows our current position
		rospy.Subscriber('/odom', Odometry, self.update_pose)

		rospy.on_shutdown(self.shutdown)

	def udp_init(self):
		ip = "" # Update IP
		server_addr = (ip, 5001)
		# Create a UDP socket
		self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		# Bind the socket to the port
		self.sock.bind(server_addr)

		# Before move turtlebot, get address and time T
		data, addr = self.sock.recvfrom(4096)
		self.addr = addr
		str_data = data.decode()
		str_array = str_data.split(';')
		self.T = float(str_array[0])

	def update_pose(self, msg):
		self.pose['x'] = msg.pose.pose.position.x
		self.pose['y'] = msg.pose.pose.position.y

		quat = [msg.pose.pose.orientation.x,
				msg.pose.pose.orientation.y,
				msg.pose.pose.orientation.z,
				msg.pose.pose.orientation.w]
		[_, _, yaw] = euler_from_quaternion(quat)
		self.pose['theta'] = self.wrap_to_pi(yaw)

	def udp_move(self):
		move_cmd = Twist()
		# Robot always excepts new controls. ctrl+C to stop
		while True:
			self.r.sleep()

			linear_speed, angular_speed = self.udp_receive()
			print("Linear: {}, Angular: {}".format(linear_speed, angular_speed))
			move_cmd.linear.x = linear_speed
			move_cmd.linear.y = 0
			move_cmd.linear.z = 0

			move_cmd.angular.x = 0
			move_cmd.angular.y = 0
			move_cmd.angular.z = angular_speed

			t_start = rospy.Time.now().to_sec()
			t_current = rospy.Time.now().to_sec()
			while t_current - t_start < self.T:
				self.cmd_vel.publish(move_cmd)
				self.r.sleep()
				t_current = rospy.Time.now().to_sec()

			self.udp_send_data()

	def udp_receive(self):
		# Receive message and move robot
		data, addr = self.sock.recvfrom(4096)
		self.addr = addr

		str_data = data.decode()
		str_array = str_data.split(';')
		linear_speed = float(str_array[0])
		angular_speed = float(str_array[1])

		return linear_speed, angular_speed

	def udp_send_data(self):
		# Send data (time,x,y,theta) to UDP
		t0 = rospy.Time.now().to_sec()
		x = self.pose['x']
		y = self.pose['y']
		theta = self.pose['theta']
		message = "{};{};{};{}".format(t0, x, y, theta)
		self.sock.sendto(message.encode(), self.addr)

	def get_distance(self, goal):
		dist = sqrt(pow((goal[0] - self.pose['x']), 2) +
					pow((goal[1] - self.pose['y']), 2))
		return dist

	def wrap_to_pi(self, angle):
		return (angle + pi) % (2 * pi) - pi

	def rotate(self, x, y, r):
		return [(x*cos(r)) - (y*sin(r)), (y*cos(r)) - (x*sin(r))]

	def shutdown(self):
		self.cmd_vel.publish(Twist())
		rospy.sleep(1)

if __name__ == '__main__':
	robot_control = MoveAround()
	robot_control.udp_move()


