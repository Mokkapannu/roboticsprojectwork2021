function [notZero] = hasZeroInput(u_cl, input_size)

if size(u_cl, 1) < input_size
    notZero = true;
    return
end

input_size = input_size -1;
u_end = u_cl(end:-1:end-input_size,:);
error = 1e-2;

for k=1:input_size
    % Check if at least one input is not zero
    if ~(abs(u_end(k,1)) < error && abs(u_end(k,2)) < error)
        notZero = true;
        return
   end
end
% Every input in u_end is zero
notZero = false;
end
