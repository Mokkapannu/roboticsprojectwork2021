function send_udp(udp, u_control)
% PARAM udp: UDP connection object
% PARAM u_control: Control inputs v and w (linear and angular speeds)
%  1x2 vector

v = sprintf('%.7f', u_control(1));
w = sprintf('%.7f', u_control(2));
str_data = [num2str(v),';',num2str(w)];

fwrite(udp, str_data)
end
