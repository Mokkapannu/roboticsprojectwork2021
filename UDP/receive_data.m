function [t0, x0] = receive_udp(udp)
% PARAM udp: UDP connection object
%
% RETURN t0: Current time (ROS)
% RETURN x0: Robot's current state (x,y,theta), 3x1 vector

msg = fscanf(udp);

% Convert string to time and states
str_vector = split(msg, ';');
t0 = str2double(str_vector(1));
x0 = str2double(str_vector(2:4));
end
