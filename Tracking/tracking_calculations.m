% Tracking calculations for Sim_4_MPC_Robot_tracking_mul_shooting.m

syms x_ref t
%%
% time start and end
t_sae = [0 48]

x(t) = 0.4*t;
% x start and end
x_sae = x(t_sae)
xd = diff(x,t);

y_ref(x_ref) = 0.5*sin(0.5*x_ref)+0.2; %-0.1*x+0.6;
% y start and end
y_sae = y_ref(x_sae)
yd = diff(y_ref,x_ref);

theta_ref(x_ref) = atan(yd)

%%
u_ref(x_ref) = sqrt(xd^2+yd(x_ref)^2)
omega_ref(x_ref) = diff(theta_ref,x_ref)
