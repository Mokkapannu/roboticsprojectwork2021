% Trajectory Tracking + Multiple shooting
clear
close all
clc

% Initialize UDP
T = 0.4; %0.5; %[s]
ip = '192.168.56.101';
udp_connection = udp(ip, 5006); % ifconfig
fopen(udp_connection);
udp_sender(udp_connection, [T,0])
%%

% CasADi v3.4.5 -> v3.5.5
addpath('C:\Users\Jussi\Casadi Matlab\casadi-windows-matlabR2016a-v3.5.5')
%addpath('./workshop_github/Codes_casadi_v3_4_5/MPC_code/') % Adds path to files
import casadi.*

N = 25; %8; % prediction horizon
rob_diam = 0.3;

v_max = 0.5; v_min = -v_max;
omega_max = pi/4; omega_min = -omega_max;

x = SX.sym('x'); y = SX.sym('y'); theta = SX.sym('theta');
states = [x;y;theta]; n_states = length(states);

v = SX.sym('v'); omega = SX.sym('omega');
controls = [v;omega]; n_controls = length(controls);
rhs = [v*cos(theta);v*sin(theta);omega]; % system r.h.s

f = Function('f',{states,controls},{rhs}); % nonlinear mapping function f(x,u)
U = SX.sym('U',n_controls,N); % Decision variables (controls)
%P = SX.sym('P',n_states + n_states);
P = SX.sym('P',n_states + N*(n_states+n_controls));
% parameters (which include the initial state and the reference along the
% predicted trajectory (reference states and reference controls))

X = SX.sym('X',n_states,(N+1));
% A vector that represents the states over the optimization problem.

obj = 0; % Objective function
g = [];  % constraints vector

% Q = 1 1 0.5 and R = 0.5 0.05
Q = zeros(3,3); Q(1,1) = 1;Q(2,2) = 1;Q(3,3) = 0.5; % weighing matrices (states)
R = zeros(2,2); R(1,1) = 0.5; R(2,2) = 0.05; % weighing matrices (controls)

st  = X(:,1); % initial state
g = [g;st-P(1:3)]; % initial condition constraints
for k = 1:N
    st = X(:,k);  con = U(:,k);
    %obj = obj+(st-P(4:6))'*Q*(st-P(4:6)) + con'*R*con; % calculate obj
    obj = obj+(st-P(5*k-1:5*k+1))'*Q*(st-P(5*k-1:5*k+1)) + ...
              (con-P(5*k+2:5*k+3))'*R*(con-P(5*k+2:5*k+3)) ; % calculate obj
    % the number 5 is (n_states+n_controls)
    st_next = X(:,k+1);
    f_value = f(st,con);
    st_next_euler = st+ (T*f_value);
    g = [g;st_next-st_next_euler]; % compute constraints
end
% make the decision variable one column  vector
OPT_variables = [reshape(X,3*(N+1),1);reshape(U,2*N,1)];

nlp_prob = struct('f', obj, 'x', OPT_variables, 'g', g, 'p', P);

opts = struct;
opts.ipopt.max_iter = 2000;
opts.ipopt.print_level =0;%0,3
opts.print_time = 0;
opts.ipopt.acceptable_tol =1e-8;
opts.ipopt.acceptable_obj_change_tol = 1e-6;

solver = nlpsol('solver', 'ipopt', nlp_prob,opts);

args = struct;

args.lbg(1:3*(N+1)) = 0;  % -1e-20  % Equality constraints
args.ubg(1:3*(N+1)) = 0;  % 1e-20   % Equality constraints

args.lbx(1:3:3*(N+1),1) = -20; %state x lower bound % new - adapt the bound
args.ubx(1:3:3*(N+1),1) = 40; %20; %state x upper bound  % new - adapt the bound
args.lbx(2:3:3*(N+1),1) = -2; %state y lower bound
args.ubx(2:3:3*(N+1),1) = 2; %state y upper bound
args.lbx(3:3:3*(N+1),1) = -inf; %state theta lower bound
args.ubx(3:3:3*(N+1),1) = inf; %state theta upper bound

args.lbx(3*(N+1)+1:2:3*(N+1)+2*N,1) = v_min; %v lower bound
args.ubx(3*(N+1)+1:2:3*(N+1)+2*N,1) = v_max; %v upper bound
args.lbx(3*(N+1)+2:2:3*(N+1)+2*N,1) = omega_min; %omega lower bound
args.ubx(3*(N+1)+2:2:3*(N+1)+2*N,1) = omega_max; %omega upper bound
%----------------------------------------------
% ALL OF THE ABOVE IS JUST A PROBLEM SET UP


% THE SIMULATION LOOP SHOULD START FROM HERE
%-------------------------------------------
t0 = 0;
x0 = [0 ; 0 ; 0.0];    % initial condition.
xs = [1.5 ; 1.5 ; 0.0]; % Reference posture.

% Get current time and position
udp_sender(udp_connection, 'time');
[t_start, ~] = udp_receiver(udp_connection);

xx(:,1) = x0; % xx contains the history of states
t(1) = t0;

u0 = zeros(N,2);        % two control inputs for each robot
X0 = repmat(x0,1,N+1)'; % initialization of the states decision variables

max_t = 70; %30; % Maximum simulation time
max_error = 1e-2;

% Start MPC
step = 0;
xx1 = [];
u_cl=[];

% the main simulaton loop... it works as long as the error is greater
% than 10^-6 and the number of mpc steps is less than its maximum
% value.
main_loop = tic;
while(norm((x0-xs),2) > max_error && step < max_t / T && hasZeroInput(u_cl, 5)) % new - condition for ending the loop
    current_time = step*T;  %new - get the current time
    % args.p   = [x0;xs]; % set the values of the parameters vector
    %----------------------------------------------------------------------
    args.p(1:3) = x0; % initial condition of the robot posture
    for k = 1:N %new - set the reference to track
        t_predict = current_time + (k-1)*T; % predicted time instant
        
        % For y = 0.1*x + 0.2, x = 0.5*t, t = [0, 24]
        %x_ref = 0.5*t_predict; y_ref = -0.1*x_ref+0.6;
        %theta_ref = atan(-0.1); % atan( y'(t) )
        %u_ref = 0.53;
        %omega_ref = 0;
        
        % For y = 0.5*sin(0.5*x)+0.2, x = 0.4*t, t = [0, 48]
        % state x upper bound 20->40
        x_ref = 0.4*t_predict;
        y_ref = 0.5*sin(0.5*x_ref)+0.2;
        theta_ref = atan(cos(x_ref/2)/4);
        u_ref = (cos(x_ref/2)^2/16 + 4/25)^(1/2);
        omega_ref = -sin(x_ref/2)/(8*(cos(x_ref/2)^2/16 + 1));

        if x_ref >= 24 % the trajectory end is reached % t = 48
            %x_ref = 12; y_ref = -0.6; theta_ref = atan(-0.1);
            x_ref = 24;
            y_ref = sin(12)/2 + 1/5;
            theta_ref = atan(cos(x_ref/2)/4);
            u_ref = 0; omega_ref = 0;
        end
        args.p(5*k-1:5*k+1) = [x_ref, y_ref, theta_ref];
        args.p(5*k+2:5*k+3) = [u_ref, omega_ref];
    end
    %----------------------------------------------------------------------    
    % initialize the optimization variables
    args.x0  = [reshape(X0',3*(N+1),1);reshape(u0',2*N,1)];
    % calculate solution
    sol = solver('x0', args.x0, 'lbx', args.lbx, 'ubx', args.ubx,...
        'lbg', args.lbg, 'ubg', args.ubg,'p',args.p);
    
    u = reshape(full(sol.x(3*(N+1)+1:end))',2,N)'; % get controls 
    
    xx1(:,1:3,step+1)= reshape(full(sol.x(1:3*(N+1)))',3,N+1)'; % trajectory
    u_cl= [u_cl ; u(1,:)];
    t(step+1) = t0-t_start;
    
    % Apply the control and shift the solution
    % T = sampling time 
    % t0 = current time 
    % x0 = current posture
    % u = control
    % f = non-linear mapping func, for testing
    
    % send values over UDP
    cal_controls = u(1,:);
    udp_sender(udp_connection, cal_controls);
    % receive updated values from robot 
    % what happens if no values available? 
    [t0, x0] = udp_receiver(udp_connection);
    
    % shift, for the sake of testing
    %[t0, x0, u0] = shift(T, t0, x0, u, f);
    % xx(:,step+2) = x0;
    
    xx(:,step+2) = x0;
    
    X0 = reshape(full(sol.x(1:3*(N+1)))',3,N+1)';
    X0 = [X0(2:end,:);X0(end,:)];
   
    step
    % update step-count
    step = step + 1;
end
main_loop_time = toc(main_loop);
final_error = norm((x0-xs),2)

Draw_Tracking(t,xx,xx1,u_cl,xs,N,rob_diam)


