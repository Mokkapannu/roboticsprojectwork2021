% Model Predictatice Control - algorithm for point reaching & obstacle
% avoidance. 

% Requires CasADi version 3.5.5 or 3.4.5.

% NOTE: Update path! 
addpath('');
import casadi.*

addpath('../UDP');
addpath('../utils');

addpath('../UDP'); % Files for data-transfer
addpath('../utils'); % Helper functions

T = 0.2; % sampling time
N = 50; % prediction horizon

% UDP iniatilization
ip = ''; % localhost or remote IP
udp_connection = udp(ip, 5001); 
fopen(udp_connection);
send_data(udp_connection, [T,0]);

% limit speed & turning angle
v_max = 0.5; v_min = -v_max;
omega_max = pi; omega_min = -omega_max;

% define states and controls
x = SX.sym('x'); y = SX.sym('y'); theta = SX.sym('theta');
states = [x;y;theta]; n_states = length(states);
v = SX.sym('v'); omega = SX.sym('omega');
controls = [v;omega]; n_controls = length(controls);

rhs = [v*cos(theta);v*sin(theta);omega]; 

% Nonlinear mapping function for cost minimization
f = Function('f',{states,controls},{rhs}); 
U = SX.sym('U',n_controls,N);  % decision variables (controls)
P = SX.sym('P',n_states + n_states); % parameters
X = SX.sym('X',n_states,(N+1)); % states

obj = 0; % cbjective function
g = [];  % constraints 

% weights
Q = zeros(3,3); Q(1,1) = 0.7; Q(2,2) = 0.7; Q(3,3) = 0.1; % states (x,y,z)
R = zeros(2,2); R(1,1) = 0.3; R(2,2) = 0.1; % controls (v,omega)

t0 = 0; % Initial time
x0 = [0; 0; 0.0]; % Initial posture
xs = [1.5; 1.5; 0]; % Goal posture


g = [g;X(:,1)-P(1:3)]; % Initial condition constraints for initial state X
for k = 1:N % loop from 1 to prediction horizon
    state = X(:,k); 
    con = U(:,k);
    obj = obj+(state-P(4:6))'*Q*(state-P(4:6)) + con'*R*con; 
    st_next = X(:,k+1);
    f_value = f(state,con);
    st_next_euler = state+ (T*f_value);
    g = [g;st_next-st_next_euler]; % Constraints
end

% Obstacles: [obs_x, obs_y, obs_diam] as a matrix
obstacle_data = [1.2, 1.2, 0.1;
                 0.8, 0.5, 0.2];
obstacles = size(obstacle_data);
rob_diam = 0.1; % Robot diameter

for i = 1:obstacles
    obs = obstacle_data(i,:);
    obs_x = obs(1); 
    obs_y = obs(2); 
    obs_diam = obs(3); 
    for k = 1:N+1  
        g = [g ; -sqrt((X(1,k)-obs_x)^2+(X(2,k)-obs_y)^2) ...
            + (rob_diam/2 + obs_diam/2)];
    end
end 

% Decision variables
OPT_variables = [reshape(X,3*(N+1),1);reshape(U,2*N,1)];
nlp_prob = struct('f', obj, 'x', OPT_variables, 'g', g, 'p', P);

% Values to restrict the algorithm
opts = struct;
opts.ipopt.max_iter = 100;
opts.ipopt.print_level = 0;
opts.print_time = 0;
opts.ipopt.acceptable_tol =1e-6; % Error tolerance
opts.ipopt.acceptable_obj_change_tol = 1e-6;

% Create solver
solver = nlpsol('solver', 'ipopt', nlp_prob,opts);

% Set constraints
args = struct;

args.lbg(1:3*(N+1)) = 0; % Equality constraints
args.ubg(1:3*(N+1)) = 0; 
args.lbg(3*(N+1)+1 : (2+obstacles)*(N+1)+ (N+1)) = -inf; % Inequality constraints
args.ubg(3*(N+1)+1 : (2+obstacles)*(N+1)+ (N+1)) = 0; 

% upper & lower bounds 
args.lbx(1:3:3*(N+1),1) = -2; args.ubx(1:3:3*(N+1),1) = 5; % x 
args.lbx(2:3:3*(N+1),1) = -2; args.ubx(2:3:3*(N+1),1) = 5; % y 
args.lbx(3:3:3*(N+1),1) = -inf; args.ubx(3:3:3*(N+1),1) = inf; % theta
args.lbx(3*(N+1)+1:2:3*(N+1)+2*N,1) = v_min; % v
args.ubx(3*(N+1)+1:2:3*(N+1)+2*N,1) = v_max; 
args.lbx(3*(N+1)+2:2:3*(N+1)+2*N,1) = omega_min; % omega  
args.ubx(3*(N+1)+2:2:3*(N+1)+2*N,1) = omega_max; 

xx(:,1) = x0; % State history 
t(1) = t0;

u0 = zeros(N,2); % Two control inputs for each robot
X0 = repmat(x0,1,N+1)'; % Initialization of the states decision variables

max_t = 10; % Maximum simulation time
max_error = 1e-2;

step = 0;
xx1 = [];
u_cl=[];

% main MPC loop
while(norm((x0-xs),2) > max_error && step < max_t / T && hasZeroInput(u_cl, 10))
    args.p   = [x0;xs]; % Parameter vector
    % Initialize the optimization variables
    args.x0  = [reshape(X0',3*(N+1),1);reshape(u0',2*N,1)];
    % calculate solution
    sol = solver('x0', args.x0, 'lbx', args.lbx, 'ubx', args.ubx,...
        'lbg', args.lbg, 'ubg', args.ubg,'p',args.p);
    
    u = reshape(full(sol.x(3*(N+1)+1:end))',2,N)'; % Controls 
   
    % trajectory & controls for draw-function
    xx1(:,1:3,step+1)= reshape(full(sol.x(1:3*(N+1)))',3,N+1)'; 
    u_cl= [u_cl ; u(1,:)]; 
    t(step+1) = t0;
    
    % Pass values to the simulation
    controls = u(1,:);
    send_data(udp_connection, controls);
    [t0, x0] = receive_data(udp_connection);
    
    xx(:,step+2) = x0;
    
    X0 = reshape(full(sol.x(1:3*(N+1)))',3,N+1)';
    X0 = [X0(2:end,:);X0(end,:)];
    step = step + 1;
end;

% Enable for MATLAB drawing
draw(t,xx,xx1,u_cl,xs,N,rob_diam,obstacle_data);

