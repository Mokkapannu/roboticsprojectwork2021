Example commands to open Turtlebot in different worlds (for example custom world), replace the world_file or world_name path with the location where you have your world saved into:

Generally:
roslaunch turtlebot_gazebo turtlebot_world.launch world_file:=<path to file> 

Example:
roslaunch turtlebot_gazebo turtlebot_world.launch world_file:=/<path>/MPC_World_V1.world
roslaunch mir_gazebo mir_empty_world.launch world_name:=<path>/MPC_World_V2.world