# MEI-56307 Robotics Project Work - Model Predictive Control for realtime robot control

This repository contain necessary files for simulating MIR robot using Model Predictive Control for path finding problem. The robot is simulated in Gazebo world and can avoid obstacles.

All the code is done as a part of coursework for Robotics Project Work - course.

## Getting Started

This section provides the information required for using the files in this repository.

### Prerequisites

All software listed below is required(*). Internet will help with installation instructions.

**MATLAB**  
The algorithm is ran on Matlab, so you'll need it too. Versions R2019b or R2020 will work, others may.
A Instrument Control Toolbox is required for UDP usage.  
**CASADI**  
 CasADi is a tool for nonlinear optimization and algorithmic differentiation. Versions v3.5.5 or 3.4.5. can be used. Make sure the installation is Matlab compatible.  
**Gazebo**  
Gazebo is a software for running the simulations. The version 11.0.0 is used here.  
**Python**  
Python is used for handling the UDP communication between Matlab and simulation. Version 3 is recommended.  

(* If you wish to only run the code on Matlab and see the result there, only first two steps of installation are required.)

## Running
Clone the repository to your local machine. All the code can be run locally, but will work on Virtual Machine as well.

Start with updating paths & IP in MPC/mpc.m - file. Add the path to your CasADi on line 7 (example: <path to folder>/casadi-osx-matlabR2015a-v3.5.5), next add your IP to line 20. Use 'localhost' if you're running everything locally and the IP of your VM if you're using one.

Start Gazebo by running the command on your terminal
```
gazebo
```

Launch the Gazebo world on new terminal (edit the file path to match the file you wish to launch)
```
roslaunch turtlebot_gazebo turtlebot_world.launch world_file:=<path to file>
```

Start Python script for communication by running the command on a new terminal
```
python MIR_Comm_MIR.py
```

Finally, Run the MPC code on Matlab and see the MIR robot move.

## Development

Any kind of Gazebo world can be created and you may test your own as you wish.
In Matlab different parameters can be tuned and changed to see the effect on the path planned.

## Authors

* **Jussi Joeperä**
* **Olli Kanerva**
* **Noora Raitanen**










